#!/bin/bash
read -p "Docker container name: " name
read -p "Minecraft Server mountpoint: " mountpoint

if [[ -f $mountpoint/docker_start.sh ]]
then
    echo Found $mountpoint/docker_start.sh, not creating a new one!
else
    read -p "Script to start: $mountpoint/" script
    echo ./$script > $mountpoint/docker_start.sh
fi

chmod +x $mountpoint/docker_start.sh

docker build . -t thearcanebrony:mcserver
docker run --name $name --mount type=bind,source=$mountpoint,target=/data --no-healthcheck -p 25565:25565 thearcanebrony:mcserver

while true do {
docker logs $mc
}
