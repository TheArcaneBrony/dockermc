# Build runtime image
FROM openjdk:14-alpine
WORKDIR /app
COPY . .
EXPOSE 25565
ENTRYPOINT ["sh", "app/startup.sh"]
